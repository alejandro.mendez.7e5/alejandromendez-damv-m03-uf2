﻿using System;
namespace ExcercicisParametritzacio
{
    public class Ejercicio5
    {
        static int[,] _tablero = new int[3,3];
        static char[] _simbolo = { ' ', 'X', 'O'};

        public static void Inicio()
        {
            bool fi;
            do
            {
                Menu();
                fi = Opcio();
            } while (!fi);
        }

        public static void Menu()
        {
            Console.WriteLine("INTRODUEIX JUGAR O SORTIR");
            Console.WriteLine("JUGAR");
            Console.WriteLine("SORTIR");
        }

        public static bool Opcio()
        {
            string op = Console.ReadLine();
            switch (op.ToUpper())
            {
                case "JUGAR":
                    Juego();
                    break;
                case "SORTIR":
                    return true;
                default: Console.WriteLine("Opcio no trobada");
                    break;
            }
            return false;
        }

        public static void Juego()
        {
            Console.WriteLine("Introduce nombre del jugador 1:");
            string p1 = Console.ReadLine();
            Console.WriteLine("Introduce nombre del jugador 2:");
            string p2 = Console.ReadLine();
            bool acaba = false;
            
            DibujarTablero();
            do
            {
                PreguntarPosicion( 1 );
                DibujarTablero();
                acaba = Ganador();
                if (acaba)
                {
                    Console.WriteLine("Ganó jugador "+p1);
                }
                else
                {
                    acaba = Empate();
                    if (acaba)
                    {
                        Console.WriteLine("Empate!");
                    }
                    else
                    {
                        PreguntarPosicion( 2 );
                        DibujarTablero();
                        acaba = Ganador();
                        if (acaba)
                        {
                            Console.WriteLine("Ganó jugador "+p2);
                        }
                    }
                }
            } while (!acaba);
        }
        
        public static void DibujarTablero()
        {
            Console.WriteLine();
            Console.WriteLine("-------------");
            for (int fila=0; fila<3; fila++)
            {
                Console.Write("|");
                for (int columna=0; columna<3; columna++)
                    Console.Write(" {0} |", _simbolo[_tablero[fila,columna]]);
                Console.WriteLine();
                Console.WriteLine("-------------");
            }
        }
        
        public static void PreguntarPosicion(int jugador)
        {
            int fila;
            int columna;
            do
            {
                fila = ComNum("En qué fila (0 a 2)");
                columna = ComNum("En qué fila (0 a 2)");
                if (_tablero[fila, columna] != 0)
                {
                    Console.WriteLine("Casilla ocupada!");
                }
            } 
            while (_tablero[fila,columna] != 0);
 
            _tablero[fila,columna] = jugador;
        }

        public static int ComNum(string mes)
        {
            int num;
            do
            {
                Console.WriteLine(mes);
                num = Convert.ToInt32(Console.ReadLine());
            } while ((num < 0) || (num > 2));

            return num;
        }
        
        public static bool Ganador()
        {
            bool raya = false;

            for (int i = 0; i < _tablero.GetLength(0); i++)
            {
                if ((_tablero[i, 0] == _tablero[i, 1]) && (_tablero[i, 0] == _tablero[i, 2]) && (_tablero[i, 0] != 0))
                {
                    raya = true;
                }
            }
            
            for (int i = 0; i < _tablero.GetLength(1); i++)
            {
                if ((_tablero[0, i] == _tablero[1, i]) && (_tablero[0, i] == _tablero[2, i]) && (_tablero[0, i] != 0))
                {
                    raya = true;
                }
            }
            
            if ((_tablero[0, 0] == _tablero[1, 1]) && (_tablero[0, 0] == _tablero[2, 2]) && (_tablero[0, 0] != 0))
            { 
                raya = true;
            }
                
            else if ((_tablero[0, 2] == _tablero[1, 1]) && (_tablero[0, 2] == _tablero[2, 0]) && (_tablero[0, 2] != 0))
            {
                raya = true;
            }
            return raya;
        }
 
 
        // ----- Devuelve "true" si hay empate 
        static bool Empate()
        {
            // Si no quedan huecos donde mover, es empate
            bool libre = false;

            for (int i = 0; i < _tablero.GetLength(0); i++)
            {
                for (int j = 0; j < _tablero.GetLength(1); j++)
                {
                    if (_tablero[i, j] == 0)
                    {
                        libre = true;
                    }
                }
            }
            return ! libre;
        }
    }
}