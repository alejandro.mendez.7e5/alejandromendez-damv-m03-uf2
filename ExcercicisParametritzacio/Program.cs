﻿using System;

namespace ExcercicisParametritzacio
{
    class Program
    {
        static void Main()
        {
            Program programa = new Program();
            programa.Inicio();
        }

        void Inicio()
        {
            bool fi;
            do
            {
                Menu();
                fi = Opcio();
            } while (!fi);
        }

        void Menu()
        {
            Console.WriteLine("RIGHTTRIANGLESIZE    1");
            Console.WriteLine("LAMP                 2");
            Console.WriteLine("CAMPSITEORGANIZER    3");
            Console.WriteLine("BASICROBOT           4");
            Console.WriteLine("THREEINAROW          5");
            Console.WriteLine("SQUASHCOUNTER        6");
            Console.WriteLine("SORTIR               0");
            
        }

        bool Opcio()
        {
            int op = Convert.ToInt32(Console.ReadLine());
            switch (op)
            {
                case 0:
                    return true;
                case 1:
                    Ejercicio1.SelTriangle();
                    break;
                case 2:
                    Ejercicio2.Inicio();
                    break;
                case 3:
                    Ejercicio3.Inicio();
                    break;
                case 4:
                    Ejercicio4.Inicio();
                    break;
                case 5:
                    Ejercicio5.Inicio();
                    break;
                case 6:
                    Ejercicio6.Inicio();
                    break;
                default: Console.WriteLine("Opcio no valida");
                    break;
            }

            return false;
        }
    }
}