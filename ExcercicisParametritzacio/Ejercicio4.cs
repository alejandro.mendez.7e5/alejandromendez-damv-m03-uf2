﻿using System;
namespace ExcercicisParametritzacio
{
    public class Ejercicio4
    {
        const double _increment = 0.5;
        public static void Inicio()
        {
            bool fi;
            var velocitat = 1.0;
            var posicioY = 0.0;
            var posicioX = 0.0;
          
            do
            {
                Menu();
                fi = Robot(ref posicioX, ref posicioY, ref velocitat);
            } while (!fi);
        }

        public static void Menu()
        {
            Console.WriteLine("INTRODUEIX EL MOVIMENT DEL ROBOT");
            Console.WriteLine("DALT / BAIX / DRETA / ESQUERRA / ACCELERAR / DESMINUIR / POSICIO / VELOCITAT / SORTIR");
        }

        public static bool Robot(ref double posicioX, ref double posicioY, ref double velocitat)
        {
            var op = Console.ReadLine();
            switch (op?.ToUpper())
            {
                case "DALT":
                case "BAIX":
                    MovVert(op, ref posicioY, ref velocitat);
                    break;
                case "DRETA":
                case "ESQUERRA":
                    MovHor(op, ref posicioX, ref velocitat);
                    break;
                case "ACCELERAR":
                    VelA(ref velocitat);
                    break;
                case "DISMINUIR":
                    VelD(ref velocitat);
                    break;
                case "POSICIO":
                    Console.WriteLine("La posició X es {0}, la posició Y es {1}", posicioX, posicioY);
                    break;
                case "VELOCITAT":
                    Console.WriteLine("La velocitat es {0} ", velocitat);
                    break;
                case "SORTIR":
                    return true;
                default: Console.WriteLine("Opcio no trobada");
                    break;
            }
            return false;
        }

        public static void MovVert(string op, ref double posicioY, ref double velocitat)
        {
            if (op == "DALT")
            {
                posicioY += velocitat;
            }
            else
            {
                posicioY -= velocitat;
            }
        }

        public static void MovHor(string op, ref double posicioX, ref double velocitat)
        {
            if (op == "DRETA")
            {
                posicioX += velocitat;
            }
            else
            {
                posicioX -= velocitat;
            }
        }

        public static void VelA(ref double velocitat)
        {
            if (velocitat <= 10)
            {
                velocitat += _increment;
            }
            else Console.WriteLine("La velocitat no pot ser superior a 10");
        }
        
        public static void VelD(ref double velocitat)
        {
            if (velocitat > 0)
            {
                velocitat += _increment;
            }
            else Console.WriteLine("La velocitat no pot ser menor a 0");
        }
    }
}