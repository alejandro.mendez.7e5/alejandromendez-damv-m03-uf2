﻿using System;
using System.Linq;

namespace ExcercicisParametritzacio
{
    public class Ejercicio3
    {
        public static void Inicio()
        {
            string[] campistes = new string[0];
            int[] pers = new int [0];
            int parceles = 0;
            bool fi;
            do
            {
                Menu();
                fi = Opcio(ref campistes, ref pers, ref parceles);
            } while (!fi);
        }

        public static void Menu()
        {
            Console.WriteLine("INTRODUEIX ENTRADA O SORTIDA Y PER SORTIR DEL PROGRAMA END");
            Console.WriteLine("ENTRADA CAMPISTES");
            Console.WriteLine("SORTIDA CAMPISTES");
        }

        public static bool Opcio(ref string[] campistes, ref int[] pers, ref int parceles)
        {
            string op = Console.ReadLine();
            switch (op.ToUpper())
            {
                case "ENTRADA":
                    IntroduirCampistes(ref campistes, ref pers, ref parceles);
                    MostrarCamping(ref pers, ref parceles);
                    break;
                case "SORTIDA":
                    TreureCampistes(ref campistes, ref pers, ref parceles);
                    MostrarCamping(ref pers, ref parceles);
                    break;
                case "END":
                    return true;
                default: Console.WriteLine("Opcio no trobada");
                    break;
            }
            return false;
        }
        
        public static void IntroduirCampistes(ref string[] campistes, ref int[] pers, ref int parceles)
        {
            string[] noms = new string[campistes.Length + 1];
            int[] num = new int[pers.Length + 1];
            parceles = +1;
            

            EntraNom(ref noms);
            NumPers(ref num);
        }

        public static void EntraNom(ref string[] noms)
        {
            Console.WriteLine("Nom de la persona que fa la reserva");
            string nom = Console.ReadLine();
            
            noms[noms.Length - 1] = nom;
        }
        
        public static void NumPers(ref int[] num)
        {
            int numero;
            do
            {
                Console.WriteLine("Quantes persones entren?");
                numero = Convert.ToInt32(Console.ReadLine());
            } while (numero <= 0);
           
            num[num.Length - 1] = numero;
        }

        public static void TreureCampistes(ref string[] campistes, ref int[] pers, ref int parceles)
        {
            if (campistes.Length == 0)
            {
                Console.WriteLine("No hi ha cap campista");
            }
            else
            {
                int posicio;
                posicio = EliminarNom(ref campistes);
                EliminarNumero(ref pers, posicio);
            }

            parceles = -1;
        }

        public static int EliminarNom(ref string[] campistes)
        {
            
            string campista = Console.ReadLine();
            
            var posicio = 0;

            if (campistes.Contains(campista))
            {
                string[] eliminacio = new string[campistes.Length - 1];

                int aux = 0;
                foreach (var i in campistes)
                {
                    if (i != campista)
                    {
                        eliminacio[aux] = i;
                        aux++;
                    }
                    else posicio = aux;
                }
                campistes = eliminacio;
                return posicio;
            }
            Console.WriteLine("No hi ha cap campista registrat amb aquest nom");
            return -1;
        }

        public static void EliminarNumero(ref int[] pers, int posicio)
        {
            if (posicio != -1)
            {
                int[] eliminacio = new int[pers.Length - 1];
                
                int aV = 0; 
                int aN = 0; 
               
                foreach (var element in pers)
                {
                    if (aV != posicio)
                    {
                        eliminacio[aN] = element;
                        aN++;
                    }
                    aV++;
                }
                pers = eliminacio;
            }
        }
        
        public static void MostrarCamping(ref int[] pers, ref int parceles)
        {
            Console.WriteLine("En el camping hi ha {0} campistes, hi {1} parceles ocupades",pers.Sum(), parceles);
        }
    }
}