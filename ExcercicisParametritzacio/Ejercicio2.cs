using System;

namespace ExcercicisParametritzacio
{
    public class Ejercicio2
    {
        public static void Inicio()
        {
            bool fi;
            do
            {
                fi = Opcio();
            } while (!fi);
        }
        public static bool Opcio()
        {
            Console.WriteLine("LAMPARA / SORTIR");
            string op = Console.ReadLine();
            switch (op.ToUpper())
            {
                case "LAMPARA":
                    Color();
                    break;
                case "SORTIR":
                    return true;
                default: Console.WriteLine("Opcio no trobada");
                    break;
            }
            return false;
        }

        public static void Color()
        {
            bool lampara = false;
            Console.WriteLine("TURN ON / TURN OFF / TOGGLE / TORNA");
            lampara = Lamp("Introdueix l'estat de la llampara");
            Console.WriteLine(lampara);
        }

        public static bool Lamp(string mes) 
        {
            bool lamp = false; 
            Console.WriteLine(mes); 
            string op = Console.ReadLine();
            
            switch (op.ToUpper()) 
            { 
                case "TURN ON": 
                    lamp = true; 
                    break;
                case "TURN OFF": 
                    lamp = false; 
                    break;
                case "TOGGLE": 
                    if (lamp) 
                    {
                        lamp = false;
                    }
                    else 
                    { 
                        lamp = true;
                    } 
                    break;
                case "TORNA": 
                    Opcio(); 
                    break;
                default: Console.WriteLine("Opcio no trobada"); 
                    break;
            } 
                return lamp; 
        } 
    }
}