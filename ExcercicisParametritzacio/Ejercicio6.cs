﻿using System;
namespace ExcercicisParametritzacio
{
    public class Ejercicio6
    {
        public static void Inicio()
        {
            bool fi;
            do
            {
                Menu();
                fi = Opcio();
            } while (!fi);
        }

        public static void Menu()
        {
            Console.WriteLine("INTRODUEIX SQUASH O SORTIR");
            Console.WriteLine("SQUASH");
            Console.WriteLine("SORTIR");
        }

        public static bool Opcio()
        {
            string op = Console.ReadLine();
            switch (op.ToUpper())
            {
                case "SQUASH":
                    Marcador();
                    break;
                case "SORTIR":
                    return true;
                default: Console.WriteLine("Opcio no trobada");
                    break;
            }
            return false;
        }

        public static void Marcador()
        {
            int contador = 1;
            int num = IntroudirDadesPartit();
            int equip1 = 0;
            int equip2 = 0;
            int seta = 0;
            int setb = 0;
            int a;
            int b;
            while (contador <= num)
            {
                a = 0;
                b = 0;
                IntroudirMarcador(ref a, ref b);
                if (a == b)
                {
                    Console.WriteLine("\n No pot ser empat");
                    IntroudirMarcador(ref a, ref b);
                }
                if (a > b) seta++;
                else if (b > a) setb++;
                Console.WriteLine("\n {0} - {1}", a, b);
                if (seta == 3)
                {
                    Partit(ref equip1,ref equip2, ref seta, ref setb);
                    seta = 0;
                }else if (setb == 3)
                {
                    Partit(ref equip1,ref equip2, ref seta, ref setb);
                    setb = 0;
                }
                contador++;
            }

            Console.WriteLine("{0} - {1}", equip1, equip2);
        }

        public static void Partit(ref int equip1, ref int equip2, ref int seta, ref int setb )
        {
            if (seta == 3) equip1 += 1;
            else if (setb == 3) equip2+=1;
        }

        public static void IntroudirMarcador(ref int a, ref int b)
        {
            char marcador;
            Console.WriteLine("Introduce los puntos que gana cada equipo");
            do
            {
                marcador = (char) Console.Read();
                Recuento(ref marcador, ref a, ref b);
                
            } while (marcador != 'F');
        }
        public static int IntroudirDadesPartit()
        {
            Console.WriteLine("Introdueix quants partits es jugaran");
            int n = Convert.ToInt32(Console.ReadLine());
            return n;
        }

        public static void Recuento(ref char marcador, ref int a, ref int b)
        {
            
            if (marcador == 'A')
            {
                a++;
            }
            else if (marcador == 'B')
            {
                b++;
            }
        }
    }
}