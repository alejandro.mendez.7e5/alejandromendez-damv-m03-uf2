using System;

namespace ExcercicisParametritzacio
{
    public class Ejercicio1
    {
        private static double _cantidad = EnterPositiu("Cuantos triangulos");
        public static void SelTriangle()
        {
            Console.WriteLine("Selecciona si es un EQULATERO / ISOSCELES / RECTANGULO");
            string opcio = Console.ReadLine();
            switch (opcio.ToUpper())
            {
                case "EQUILATERO":
                    Equilatero();
                    break;
                case "ISOSCELES":
                    Isosceles();
                    break;
                case "RECTANGULO":
                    Rectangulo();
                    break;
                default: Console.WriteLine("Opcio no valida");
                    break;
            }
        }
        public static double[] IntroduirCantidad1()
        {
            double[] medidasB = new double[(int) (_cantidad)];
            
            for (int i = 0; i < medidasB.Length; i++)
            {
                medidasB[i] = EnterPositiu("Introdueix la base");
            }

            return medidasB;
        }

        public static double[] IntroduirCantidad2()
        {
            double[] medidasA = new double[(int) (_cantidad)];
            for (int i = 0; i < medidasA.Length; i++)
            {
                medidasA[i] = EnterPositiu("Introdueix l'altura");
            }

            return medidasA;
        }
        
//Math SQRT
        public static void Equilatero()
        {
            double[] medidasB2 = IntroduirCantidad1();
            double[] medidasA2 = IntroduirCantidad2();
            
            double area;
            double perimetro;
            for (int i = 0; i < medidasB2.Length; i++)
            {
                for (int j = 0; j < medidasA2.Length; j++)
                {
                    area = (medidasB2[i] * medidasA2[j]) / 2;

                    Console.WriteLine("L'area dels triangres son " + area);
                }
            }
            
            for (int i = 0; i < medidasA2.Length; i++)
            {
                perimetro = medidasA2[i]*3;
                Console.WriteLine("El perimetro dels triangles son " + perimetro);
            }
        }

        public static void Isosceles()
        {
            double[] medidasB2 = IntroduirCantidad1();
            double[] medidasA2 = IntroduirCantidad2();
            
            double area = 0;
            double perimetro = 0;
            for (int i = 0; i < medidasB2.Length; i++)
            {
                for (int j = 0; j < medidasA2.Length; j++)
                {
                    area = Math.Sqrt((medidasB2[i] * medidasB2[i]) + (medidasA2[j] * medidasA2[j]));
                    perimetro = (area * 2) + medidasB2[i];
                }
                Console.WriteLine("L'area dels isosceles son " + area+ " i el perimetre de "+perimetro);
            }
        }
        
        public static void Rectangulo()
        {
            double[] medidasB2 = IntroduirCantidad1();
            double[] medidasA2 = IntroduirCantidad2();
            
            double area = 0;
            double perimetro = 0;
            for (int i = 0; i < medidasB2.Length; i++)
            {
                for (int j = 0; j < medidasA2.Length; j++)
                {
                    area = Math.Sqrt((medidasB2[i] * medidasB2[i]) + (medidasA2[j] * medidasA2[j]));
                    perimetro = medidasB2[i] + medidasA2[j] + area;
                }
                Console.WriteLine("L'area dels isosceles son " + area+ " i el perimetre de "+perimetro);
            }
        }

        public static double EnterPositiu(string mes)
        {
            double num;
            do
            {
                Console.WriteLine(mes);
                num = Convert.ToInt32(Console.ReadLine());
            } while (num < 0);

            return num;
        }
    }
}